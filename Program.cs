﻿using System;

namespace Lesson1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 1; i < 100; i++)
            {
                Console.WriteLine(ConvertFizzBuzz(i));
            }
        }
        static public string ConvertFizzBuzz(int num)
        {
            string output = "";
            if(num % 3 == 0)
            {
                output = output + "Fizz";
            }
            if(num % 5 == 0)
            {
                output = output + "Buzz";
            }
            if(output == ""){
                output = num.ToString();
            }
            return output;
        }
    }
}
